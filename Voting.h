// --------------------------------
// /cs371p-voting/Voting.h
// Copyright (C) 2018
// David Vu
// --------------------------------


#ifndef Voting_h
#define Voting_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <vector>

using namespace std;

/**
 * print winner
 * @param w an ostream
 * @param winner name of winner
 */
void voting_print (ostream& w, string& winner);

// -------------
// voting_solve
// -------------

/**
 * solve the voting problem
 * @param r an istream
 * @param w an ostream
 */
void voting_solve (istream& r, ostream& w);

#endif // voting_h


/**
 * parse the ballot information from a string
 * @param s ballot string
 * @param output_array output result
 * @param num_candidates number of candidates participating
 */
void get_ballot(string& s, int* output_array, size_t num_candidates);


/**
 * return a list of winning candidates
 * @param winning_ballots an array of vectors, each index corresponds to a candidate
 *				            winning_ballots[i] == a list of winning ballots of
 *				            that corresponding candidate
 * @param num_candidates number of candidates participating
 * @param num_ballots number of ballots
 */
vector<int> voting_eval(vector<int*>* winning_ballots,
                        size_t num_candidates, size_t num_ballots);