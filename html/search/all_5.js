var searchData=
[
  ['voting_2ec_2b_2b',['Voting.c++',['../Voting_8c_09_09.html',1,'']]],
  ['voting_2eh',['Voting.h',['../Voting_8h.html',1,'']]],
  ['voting_5feval',['voting_eval',['../Voting_8c_09_09.html#a0c43eaa107a640c2521dce264b5d6f5e',1,'voting_eval(vector&lt; int *&gt; *winning_ballots, size_t num_candidates, size_t num_ballots):&#160;Voting.c++'],['../Voting_8h.html#a0c43eaa107a640c2521dce264b5d6f5e',1,'voting_eval(vector&lt; int *&gt; *winning_ballots, size_t num_candidates, size_t num_ballots):&#160;Voting.c++']]],
  ['voting_5fprint',['voting_print',['../Voting_8c_09_09.html#a08830a26fee4ddf5aa778cb77474a915',1,'voting_print(ostream &amp;w, string &amp;winner):&#160;Voting.c++'],['../Voting_8h.html#a08830a26fee4ddf5aa778cb77474a915',1,'voting_print(ostream &amp;w, string &amp;winner):&#160;Voting.c++']]],
  ['voting_5fsolve',['voting_solve',['../Voting_8c_09_09.html#aed21eca169108fbf5b77db1e7e21af2e',1,'voting_solve(istream &amp;r, ostream &amp;w):&#160;Voting.c++'],['../Voting_8h.html#aed21eca169108fbf5b77db1e7e21af2e',1,'voting_solve(istream &amp;r, ostream &amp;w):&#160;Voting.c++']]]
];
