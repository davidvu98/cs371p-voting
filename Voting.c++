// --------------------------------
// /cs371p-voting/Voting.c++
// Copyright (C) 2018
// David Vu
// --------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <algorithm> // std::min_element, std::max_element
#include "Voting.h"
#include <map>    // map

using namespace std;

#define MAX_BALLOTS 1000
#define MAX_CANDIDATES 20

// -------------
// get_ballot
// -------------

void get_ballot(string& s, int* output_array, size_t num_candidates)
{
    assert(output_array != NULL);
    istringstream f(s);
    string token;
    size_t i = 0;
    while(i < num_candidates)
    {
        getline(f, token, ' ');
        assert(token != "");
        output_array[i] = stoi(token);
        ++i;
    }
}


// -------------
// get_votes_count
// -------------

void get_votes_count(vector<int*>* winning_ballots, int* votes_count, size_t num_candidates)
{
    assert(votes_count != NULL);
    for(size_t i = 0; i < num_candidates; ++i)
    {
        votes_count[i] = (int) winning_ballots[i].size();
    }

}

// -------------
// mark_eliminated
// -------------


/*
    @param losers: list of eliminated candidates
    @param votes_count: candidates votes count matrix
    Set the votes count of eliminated candidates to 0
*/
void mark_eliminated(vector<int>& losers, int* votes_count)
{
    assert(votes_count != NULL);
    vector<int>::iterator it = losers.begin();
    while(it != losers.end())
    {
        votes_count[*it - 1] = 0;
        ++it;
    }
}

// -------------
// update_votes_map
// -------------

/**
 *  @param votes_map: list of eliminated candidates
 *  @param votes_count: candidates votes count matrix
 *      Set the votes count of eliminated candidates to 0
 */
void update_votes_map(map<size_t, vector<int>>& votes_map, int* votes_count, size_t num_candidates)
{
    assert(votes_count != NULL);
    votes_map.clear();
    for(size_t i = 0; i < num_candidates; ++i)
    {
        if(votes_count[i] > 0)
            votes_map[votes_count[i]].push_back(i+1);
    }
}

// -------------
// voting_print
// -------------

void voting_print (ostream& w, string& winner)
{
    w << winner << endl;
}


vector<int> voting_eval(vector<int*>* winning_ballots, size_t num_candidates, size_t num_ballots)
{
    /*create a votes_count matrix for each candidate
          votes_count[i] == candidate(i+1)'s number of votes
    */
    int votes_count[num_candidates];
    get_votes_count(winning_ballots, votes_count, num_candidates);

    /* mapping # of votes -> list of candidates who share the same # of votes
       votes_map ignore candidates with 0 vote*/
    map<size_t, vector<int>> votes_map;
    update_votes_map(votes_map, votes_count, num_candidates);


    /*while there is no candidate with more than 50% of the votes
      and no tie*/
    while(votes_map.size() > 1
            && votes_map.rbegin()->first <= (num_ballots / 2))
    {
        //grab losers with the least votes
        vector<int>& losers = votes_map.begin()->second;
        mark_eliminated(losers, votes_count);

        vector<int>::iterator it = losers.begin();
        while(it != losers.end()) //for each loser
        {
            int loser = *it;
            //for each ballot of that loser
            vector<int*>& loser_ballots = winning_ballots[loser-1];
            for(size_t j = 0; j < loser_ballots.size(); j++)
            {
                int* ballot = loser_ballots[j];

                //re-assign loser's ballots to remaining candidates
                for(size_t i = 1; i < num_candidates; ++i)
                {
                    if(votes_count[ballot[i] - 1] != 0) //not eliminated
                    {
                        //move ballot to a non eliminated candidate
                        ++votes_count[ballot[i] - 1];
                        winning_ballots[ballot[i] - 1].push_back(ballot);
                        break;
                    }
                }
            }
            ++it;
        }
        /* rebuild the votes_map with new votes_count */
        update_votes_map(votes_map, votes_count, num_candidates);
    }
    return (votes_map.rbegin())->second; //winners
}


// -------------
// voting_solve
// -------------

void voting_solve (istream& r, ostream& w) {
    string s;

    //parse first line
    getline(r,s);
    int num_tests = stoi(s);

    getline(r,s); //ignore blank line
    while(num_tests-- > 0)
    {
        getline(r,s); //get num candidates
        size_t num_candidates = (size_t) stoi(s);
        assert(num_candidates > 0 && num_candidates <= MAX_CANDIDATES);

        /*candidates' names */
        string candidates[num_candidates];
        /*buffer for all the ballots*/
        int ballots[MAX_BALLOTS][num_candidates];

        size_t num_ballots = 0;
        size_t i = 0;
        while(i < num_candidates)
        {
            getline(r, candidates[i]);
            ++i;
        } //done parsing candidates

        /*
            an array of vectors, each index corresponds to a candidate
            winning_ballots[i] == a list of winning ballots of
            that corresponding candidate
        */
        vector<int*> winning_ballots[num_candidates];

        //parsing ballots
        while(getline(r,s) && s != "")
        {
            int* ballot = ballots[num_ballots];
            get_ballot(s, ballot, num_candidates);
            winning_ballots[ballot[0]-1].push_back(ballot);
            ++num_ballots;
            assert(num_ballots <= MAX_BALLOTS);
        }

        //get the winners
        vector<int> winners = voting_eval(winning_ballots, num_candidates, num_ballots);
        for(size_t i = 0; i < winners.size(); ++i)
        {
            voting_print(w, candidates[winners[i] - 1]);
        }

        if(num_tests > 0)
            w << endl;
    }
}