// --------------------------------
// /cs371p-voting/RunVoting.c++
// Copyright (C) 2018
// David Vu
// --------------------------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Voting.h"

// ----
// main
// ----

int main () {
    using namespace std;
    voting_solve(cin, cout);
    return 0;
}
