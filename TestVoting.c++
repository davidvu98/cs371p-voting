// ------------------------------------
// /cs371p-voting/TestVoting.c++
// Copyright (C) 2018
// David Vu
// ------------------------------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string

#include "gtest/gtest.h"

#include "Voting.h"

using namespace std;

// -----------
// TestVoting
// -----------

// ----
// read
// ----

TEST(VotingFixture, read) {
    string s("1 2 3 4\n");
    int output_array[4];
    int expected_output[4] = {1,2,3,4};
    get_ballot(s, output_array,4);
    ASSERT_TRUE(equal(output_array,output_array+4,expected_output));
}


TEST(VotingFixture, read2) {
    string s("3 4 5 6\n");
    int output_array[4];
    int expected_output[4] = {3,4,5,6};
    get_ballot(s, output_array,4);
    ASSERT_TRUE(equal(output_array,output_array+4,expected_output));
}


// ----
// evaluate
// ----

TEST(VotingFixture, eval) {
    size_t num_candidates = 4;
    vector<int*> winning_ballots[num_candidates];
    int ballot1[num_candidates] = {1,2,3,4};
    int ballot2[num_candidates] = {1,2,3,4};
    winning_ballots[0].push_back(ballot1);
    winning_ballots[0].push_back(ballot2);
    int ballot3[num_candidates] = {2,1,3,4};
    int ballot4[num_candidates] = {2,1,3,4};
    winning_ballots[1].push_back(ballot3);
    winning_ballots[1].push_back(ballot4);
    int ballot5[num_candidates] = {3,1,2,4};
    int ballot6[num_candidates] = {4,2,1,4};
    winning_ballots[2].push_back(ballot5);
    winning_ballots[3].push_back(ballot6);

    int expected_output[2] = {1,2};
    vector<int> output = voting_eval(winning_ballots, num_candidates, 6);
    ASSERT_TRUE(equal(output.begin(),output.begin()+output.size(),expected_output));
}


// ----
// evaluate
// ----

TEST(VotingFixture, eval2) {
    size_t num_candidates = 4;
    vector<int*> winning_ballots[num_candidates];
    int ballot1[num_candidates] = {1,2,3,4};
    int ballot2[num_candidates] = {1,2,3,4};
    int ballot3[num_candidates] = {1,1,3,4};
    winning_ballots[0].push_back(ballot1);
    winning_ballots[0].push_back(ballot2);
    winning_ballots[0].push_back(ballot3);
    int ballot4[num_candidates] = {2,1,3,4};
    winning_ballots[1].push_back(ballot4);
    int ballot5[num_candidates] = {3,1,2,4};
    int ballot6[num_candidates] = {4,2,1,4};
    winning_ballots[2].push_back(ballot5);
    winning_ballots[3].push_back(ballot6);

    int expected_output[2] = {1};
    vector<int> output = voting_eval(winning_ballots, num_candidates, 6);
    ASSERT_TRUE(equal(output.begin(),output.begin()+output.size(),expected_output));
}


// ----
// print
// ----

TEST(VotingFixture, print) {
    ostringstream w;
    string s("Person0");
    voting_print(w,s);
    ASSERT_EQ(w.str(), "Person0\n");
}

// -----
// solve
// -----

TEST(VotingFixture, solve) {
    istringstream r("1\n\n4\nPerson1\nPerson2\nPerson3\nPerson4\n1 2 3 4\n1 2 3 4\n1 2 3 4\n1 2 3 4\n2 1 3 4\n2 1 3 4\n2 1 3 4\n2 1 3 4\n3 4 1 2\n4 2 3 1\n");
    ostringstream w;
    voting_solve(r, w);
    ASSERT_EQ("Person1\nPerson2\n", w.str());
}

TEST(VotingFixture, solve2) {
    istringstream r("1\n\n4\nPerson1\nPerson2\nPerson3\nPerson4\n1 2 3 4\n1 2 3 4\n1 2 3 4\n1 2 3 4\n2 1 3 4\n2 1 3 4\n2 1 3 4\n2 1 3 4\n3 4 1 2\n4 3 2 1\n");
    ostringstream w;
    voting_solve(r, w);
    ASSERT_EQ("Person1\nPerson2\n", w.str());
}

TEST(VotingFixture, solve3) {
    istringstream r("1\n\n4\nPerson1\nPerson2\nPerson3\nPerson4\n1 2 3 4\n1 2 3 4\n1 2 3 4\n1 2 3 4\n2 1 3 4\n2 1 3 4\n2 1 3 4\n2 1 3 4\n3 4 2 1\n4 3 2 1\n");
    ostringstream w;
    voting_solve(r, w);
    ASSERT_EQ("Person2\n", w.str());
}

TEST(VotingFixture, solve4) {
    istringstream r("1\n\n4\nPerson1\nPerson2\nPerson3\nPerson4\n1 2 3 4\n1 2 3 4\n1 2 3 4\n2 1 3 4\n2 1 3 4\n2 1 3 4\n2 1 3 4\n3 4 2 1\n4 3 2 1\n");
    ostringstream w;
    voting_solve(r, w);
    ASSERT_EQ("Person2\n", w.str());
}

TEST(VotingFixture, solve5) {
    istringstream r("1\n\n4\nPerson1\nPerson2\nPerson3\nPerson4\n1 2 3 4\n1 2 3 4\n1 2 3 4\n");
    ostringstream w;
    voting_solve(r, w);
    ASSERT_EQ("Person1\n", w.str());
}

TEST(VotingFixture, solve6) {
    istringstream r("2\n\n4\nPerson1\nPerson2\nPerson3\nPerson4\n1 2 3 4\n1 2 3 4\n1 2 3 4\n\n4\nPerson1\nPerson2\nPerson3\nPerson4\n1 2 3 4\n1 2 3 4\n1 2 3 4\n2 1 3 4\n2 1 3 4\n2 1 3 4\n2 1 3 4\n3 4 2 1\n4 3 2 1\n");
    ostringstream w;
    voting_solve(r, w);
    ASSERT_EQ("Person1\n\nPerson2\n", w.str());
}


TEST(VotingFixture, solve7) {
    istringstream r("1\n\n4\nPerson1\nPerson2\nPerson3\nPerson4\n1 2 3 4\n2 1 3 4\n3 1 4 2\n4 1 2 3\n");
    ostringstream w;
    voting_solve(r, w);
    ASSERT_EQ("Person1\nPerson2\nPerson3\nPerson4\n", w.str());
}